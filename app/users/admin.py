from django.contrib import admin
from app.users.models import User

# Register your models here.


class UsersAdmin(admin.ModelAdmin):
    ...

admin.register(User, UsersAdmin)
