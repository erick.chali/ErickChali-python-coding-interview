from rest_framework.generics import ListAPIView

from app.users.models import User
from app.users.serializers import UserSerializer


class ListUsers(ListAPIView):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()

