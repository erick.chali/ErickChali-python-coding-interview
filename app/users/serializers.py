from rest_framework import serializers

from app.users.models import User


class UserSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ["name", "email", "age"]

    def get_age(self, instance):
        print(instance.age)
        return f"{instance.age} years"
